# Arabic Language support in F123Ligth 
This project will add Arabic language support in F123Ligth. we can divide the project into there stages.
* Add Arabic Language keymap and charset support in F123Ligth 
* Add Arabic Language support in CLI applications.
* Add Arabic Language support in GUI applications.

## Stage-1 (Add Arabic Language keymap and charset support in F123Ligth )
In the first Stage, we'll add Arabic keymap to F123Ligth, Also we'll change Locale language to Arabic. you can follow following instructions to set up stage-1.

### Instructions
*, First of all, we'll add Arabic keymap into F123Ligth. first copy `ar.map.gz` from stage-1 folder of this project into `/usr/share/kbd/keymaps/i386/qwerty/`.( you might need to use `sudo cp` for that.)
> You can find keymap Layout from [here](https://docs.microsoft.com/en-us/globalization/keyboards/kbda1.html)
* Now we'll set the Arabic language as system language.
* In F123Ligth we don't have Arabic as an option for system language. So we'll add Arabic into system language option.
* First edit `/etc/locale.gen` edit line 62 and  remove # from it. it'll allow `ar_SA.UTF-8` as system language.
now run `locale-gen` to finalize changes.
* Now we can chnage default language and default keymap. For that we need to edit `/etc/vconsole.conf` chnage `KEYMAP=ar`. also we need to change `LANG` enviorntmant variable to `ar_EG.utf8` by using `export LANG=ar_EG.utf8`. 
* Now restart `vconsole.service` using ` sudo systemctl restart vconsole.service`. 